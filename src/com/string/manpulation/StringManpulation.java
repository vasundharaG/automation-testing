package com.string.manpulation;

import java.lang.String;

public class StringManpulation {

	public static void main(String[] args) {
		
		StringManpulation sm = new StringManpulation();
		
//		sm.methodOne();
//		sm.secondMethod();
//		StringManpulation.stringManipulations();
		//sm.ifElseTest();
		//sm.switchTest(2);
		sm.printSequence();
	}
	
	void methodOne() {
		System.out.println("This is first Method");
		for (int i=0; i<10; i++) {
			
			System.out.println(i);			
		}					
	}
	
	 void secondMethod() {
		System.out.println("This is Second Method");
		int i=0;
		while(i<10) {
			System.out.println(i);
			i++;
		}
		
	}
	
	 static void stringManipulations() {
		
		String s ="GANGA";
		String[] rivers = {"GANGA","YAMUNA","SARASWATHI","GODAVARI", "KRISHNA"};
		
		for(int i =0; i<=rivers.length-1;i++) {
			System.out.println(rivers[i]);
			
		}
		
	}
	 
        void switchTest(int x) {
	   	
        	switch(x) {
        	case 1: 
        	System.out.println(x+" one");
        	break;
        	case 2:
        		System.out.println(x+" Two");
        	break;	
        	case 5:
        		System.out.println(x+" Five");
        	break;
        	default:
        		System.out.println(x+" This is default");
        	break;
        	}
	
			
		}
        
        void printSequence() {
        	
        	for(int i =1; i<=5; i++) {
        		
        		for(int j=1; j<=i; j++) {
        			
        			System.out.print(j+" ");
        		}
        		
        		System.out.println("");
        		
        	}
        	
        }
			

}
